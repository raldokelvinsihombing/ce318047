const { chromium } = require('playwright');

(async () => {
  const browser = await chromium.launch({
    headless: false
  });
  const context = await browser.newContext();

  // Open new page
  const page = await context.newPage();

  // Go to https://mss.cloud.javan.co.id/auth/login
  await page.goto('https://mss.cloud.javan.co.id/auth/login');

  // Click [placeholder="Email or Username"]
  await page.click('[placeholder="Email or Username"]');

  // Fill [placeholder="Email or Username"]
  await page.fill('[placeholder="Email or Username"]', 'admin@laravolt.dev');

  // Press Tab
  await page.press('[placeholder="Email or Username"]', 'Tab');

  // Fill [placeholder="Password"]
  await page.fill('[placeholder="Password"]', 'Asdf1234');

  // Press Enter
  await page.press('[placeholder="Password"]', 'Enter');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/my-kpi');

  // Click text=Master KPI
  await page.click('text=Master KPI');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/master-kpi-category');

  // Click text=Category Item KPI >> :nth-match(div, 2)
  await page.click('text=Category Item KPI >> :nth-match(div, 2)');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/master-kpi-item');

  // Click text=Filter Frequency YearlyKuadrimesterQuarterSemesterMonthlyFilter FrequencyYearlyK >> div
  await page.click('text=Filter Frequency YearlyKuadrimesterQuarterSemesterMonthlyFilter FrequencyYearlyK >> div');

  // Click :nth-match(:text("Quarter"), 2)
  await page.click(':nth-match(:text("Quarter"), 2)');

  // Click text=No Category KPI Item KPI Definition Unit Frequency Success Status Professional P >> button
  await page.click('text=No Category KPI Item KPI Definition Unit Frequency Success Status Professional P >> button');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/master-kpi-item?filter%5Bname%5D=&filter%5Bdefinition%5D=&filter%5Bmd_unit_code%5D=&filter%5Bmd_frequency_code%5D=6CE8415E-8B9F-41D1-BF5F-D57626642658&filter%5Bmd_success_code%5D=');

  // Click text=5 ODS Main KPI Thematic & Festive Promo/Campaign Memastikan promo/campaign dapat >> i
  await page.click('text=5 ODS Main KPI Thematic & Festive Promo/Campaign Memastikan promo/campaign dapat >> i');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/kpi-item/78126233-C4C7-4DBD-BC71-807EBF14B4A2/edit');

  // Click text=Save
  await page.click('text=Save');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/master-kpi-item');

  // Click text=Input KPI
  await page.click('text=Input KPI');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/kpi-input');

  // Click text=Add
  await page.click('text=Add');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/kpi-input/create');

  // Click button[role="button"]:has-text("Input KPI")
  await page.click('button[role="button"]:has-text("Input KPI")');

  // Click button[role="button"]:has-text("Submit")
  await page.click('button[role="button"]:has-text("Submit")');

  // Click text=Browse >> i
  await page.click('text=Browse >> i');

  // Upload number-of-new-commercial-partner_rawdata (5).xlsx
  await page.setInputFiles('body:has-text("Back 12 admin Edit Profil Edit Password Logout admin admin DASHBOARD Dashboard W")', 'number-of-new-commercial-partner_rawdata (5).xlsx');

  // Click text=Back
  await page.click('text=Back');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/kpi-input');

  // Click text=Master KPI
  await page.click('text=Master KPI');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/master-kpi-category');

  // Click text=Item KPI
  await page.click('text=Item KPI');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/master-kpi-item');

  // Click text=Filter Frequency YearlyKuadrimesterQuarterSemesterMonthlyFilter FrequencyYearlyK >> i
  await page.click('text=Filter Frequency YearlyKuadrimesterQuarterSemesterMonthlyFilter FrequencyYearlyK >> i');

  // Click :nth-match(:text("Quarter"), 2)
  await page.click(':nth-match(:text("Quarter"), 2)');

  // Click text=No Category KPI Item KPI Definition Unit Frequency Success Status Professional P >> button
  await page.click('text=No Category KPI Item KPI Definition Unit Frequency Success Status Professional P >> button');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/master-kpi-item?filter%5Bname%5D=&filter%5Bdefinition%5D=&filter%5Bmd_unit_code%5D=&filter%5Bmd_frequency_code%5D=6CE8415E-8B9F-41D1-BF5F-D57626642658&filter%5Bmd_success_code%5D=');

  // Click text=3 Customer Perspective Customer Satisfaction Index Mengukur kepuasan pelanggan d >> i
  await page.click('text=3 Customer Perspective Customer Satisfaction Index Mengukur kepuasan pelanggan d >> i');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/kpi-item/96EEA63B-EFC5-48D0-8F0E-00B1CAD59A6B/edit');

  // Click text=-SUMSUM (AVG)LAST VALUESUM-SUMSUM (AVG)LAST VALUE >> input
  await page.click('text=-SUMSUM (AVG)LAST VALUESUM-SUMSUM (AVG)LAST VALUE >> input');

  // Click .menu.transition div
  await page.click('.menu.transition div');

  // Click text=Save
  await page.click('text=Save');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/master-kpi-item');

  // Click :nth-match(:text("Quarter"), 2)
  await page.click(':nth-match(:text("Quarter"), 2)');

  // Click text=No Category KPI Item KPI Definition Unit Frequency Success Status Professional P >> button
  await page.click('text=No Category KPI Item KPI Definition Unit Frequency Success Status Professional P >> button');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/master-kpi-item?filter%5Bname%5D=&filter%5Bdefinition%5D=&filter%5Bmd_unit_code%5D=&filter%5Bmd_frequency_code%5D=6CE8415E-8B9F-41D1-BF5F-D57626642658&filter%5Bmd_success_code%5D=');

  // Click text=3 Customer Perspective Customer Satisfaction Index Mengukur kepuasan pelanggan d >> i
  await page.click('text=3 Customer Perspective Customer Satisfaction Index Mengukur kepuasan pelanggan d >> i');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/kpi-item/96EEA63B-EFC5-48D0-8F0E-00B1CAD59A6B/edit');

  // Click text=Save
  await page.click('text=Save');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/master-kpi-item');

  // Click text=Input KPI
  await page.click('text=Input KPI');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/kpi-input');

  // Click text=Add
  await page.click('text=Add');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/kpi-input/create');

  // Click button[role="button"]:has-text("Input KPI")
  await page.click('button[role="button"]:has-text("Input KPI")');

  // Click button[role="button"]:has-text("Submit")
  await page.click('button[role="button"]:has-text("Submit")');

  // Click text=Browse
  await page.click('text=Browse');

  // Upload number-of-new-commercial-partner_rawdata (5).xlsx
  await page.setInputFiles('body:has-text("Back 12 admin Edit Profil Edit Password Logout admin admin DASHBOARD Dashboard W")', 'number-of-new-commercial-partner_rawdata (5).xlsx');

  // Click text=Back
  await page.click('text=Back');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/kpi-input');

  // Click text=Master KPI
  await page.click('text=Master KPI');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/master-kpi-category');

  // Click text=Item KPI
  await page.click('text=Item KPI');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/master-kpi-item');

  // Click :nth-match(:text("Quarter"), 2)
  await page.click(':nth-match(:text("Quarter"), 2)');

  // Click text=No Category KPI Item KPI Definition Unit Frequency Success Status Professional P >> button
  await page.click('text=No Category KPI Item KPI Definition Unit Frequency Success Status Professional P >> button');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/master-kpi-item?filter%5Bname%5D=&filter%5Bdefinition%5D=&filter%5Bmd_unit_code%5D=&filter%5Bmd_frequency_code%5D=6CE8415E-8B9F-41D1-BF5F-D57626642658&filter%5Bmd_success_code%5D=');

  // Click text=3 Customer Perspective Customer Satisfaction Index Mengukur kepuasan pelanggan d >> i
  await page.click('text=3 Customer Perspective Customer Satisfaction Index Mengukur kepuasan pelanggan d >> i');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/kpi-item/96EEA63B-EFC5-48D0-8F0E-00B1CAD59A6B/edit');

  // Click text=-SUMSUM (AVG)LAST VALUE--SUMSUM (AVG)LAST VALUE >> input
  await page.click('text=-SUMSUM (AVG)LAST VALUE--SUMSUM (AVG)LAST VALUE >> input');

  // Click :nth-match(:text("SUM (AVG)"), 2)
  await page.click(':nth-match(:text("SUM (AVG)"), 2)');

  // Click text=Save
  await page.click('text=Save');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/master-kpi-item');

  // Click text=Input KPI
  await page.click('text=Input KPI');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/kpi-input');

  // Click text=Add
  await page.click('text=Add');
  // assert.equal(page.url(), 'https://mss.cloud.javan.co.id/modules/kpi-input/create');

  // Click button[role="button"]:has-text("Input KPI")
  await page.click('button[role="button"]:has-text("Input KPI")');

  // Click text=Browse
  await page.click('text=Browse');

  // Upload number-of-new-commercial-partner_rawdata (5).xlsx
  await page.setInputFiles('body:has-text("Back 12 admin Edit Profil Edit Password Logout admin admin DASHBOARD Dashboard W")', 'number-of-new-commercial-partner_rawdata (5).xlsx');

  // ---------------------
  await context.close();
  await browser.close();
})();
